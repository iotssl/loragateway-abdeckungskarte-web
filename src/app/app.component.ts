import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  toggleLayer: Subject<string> = new Subject();
  _opened: Boolean = false;
  // toggleableLayerIds = ['IoTGateways-SH', 'Geplante IoTGateways-SH auf Fernmeldetürmen', 'Grid-10km'];

  _toggleSidebar() {
    this._opened = !this._opened;
  }

  toggleLayerF(v: string) {
    this.toggleLayer.next(v);
  }
}
