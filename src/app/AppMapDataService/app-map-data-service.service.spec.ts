import { TestBed, inject } from '@angular/core/testing';

import { AppMapDataServiceService } from './app-map-data-service.service';

describe('AppMapDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppMapDataServiceService]
    });
  });

  it('should be created', inject([AppMapDataServiceService], (service: AppMapDataServiceService) => {
    expect(service).toBeTruthy();
  }));
});
