import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppMapDataServiceService {

  constructor(private http: HttpClient) {}
  public getIoTGWJSON(): Observable<any> {
    return this.http.get('./assets/geojson/sh-iotGW.json');
  }
  public getShIotGWPlannedJSON(): Observable<any> {
    return this.http.get('./assets/geojson/sh-iotGW-planned.json');
  }
  public getShIotGWPlannedTowerJSON(): Observable<any> {
    return this.http.get('./assets/geojson/sh-iotGW-plannedTower.json');
  }
  public getGridMaskJSON(): Observable<any> {
    return this.http.get('./assets/geojson/grid-mask.json');
  }
}
