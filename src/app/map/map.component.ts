import {Component, Input, OnInit} from '@angular/core';
import {BBox, featureCollection, FeatureCollection, Units} from '@turf/helpers';
import {AppMapDataServiceService} from '../AppMapDataService/app-map-data-service.service';
import squareGrid from '@turf/square-grid';
import circle from '@turf/circle';
import {Subject} from 'rxjs';
import { Map } from 'mapbox-gl';
import bbox from '@turf/bbox';

// TODO missing onClick for elements
// TODO style Buttons

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  @Input() toggleLayer: Subject<string>;
  map: Map;

  shIotGW: String;
  shIotGWPlannedTower: String;
  shIotGWPlanned: String;
  gridMask: String;
  squareGridFC: FeatureCollection;
  large_circlesFC: FeatureCollection;
  small_circlesFC: FeatureCollection;
  large_circles_plannedTowerFC: FeatureCollection;
  small_circles_plannedTowerFC: FeatureCollection;
  large_circles_plannedFC: FeatureCollection;
  small_circles_plannedFC: FeatureCollection;
  small_circles_existing: Array<any> = [];
  large_circles_existing: Array<any> = [];
  small_circles_plannedTower: Array<any> = [];
  large_circles_plannedTower: Array<any>  = [];
  small_circles_planned: Array<any> = [];
  large_circles_planned: Array<any>  = [];
  bounds: BBox;

  layouts = {
    existing: {
      'visibility': 'visible',
    },
    planned: {
      'visibility': 'visible',
    },
    plannedTower: {
      'visibility': 'visible',
    },
    grid: {
      'visibility': 'visible',
    }
  };
  constructor(private appMapDataServiceService: AppMapDataServiceService ) {
    // this.map.setLayoutProperty('country-label-lg', 'text-field', '{name_de}');
  }
  ngOnInit() {
    this.appMapDataServiceService.getGridMaskJSON().subscribe(data => {
      this.gridMask = data;
      this.bounds = this.getBounds();

      this.addGrid();
    });
    this.appMapDataServiceService.getIoTGWJSON().subscribe(data => {
      this.shIotGW = data;

      this.addCircles();
    });
    this.appMapDataServiceService.getShIotGWPlannedTowerJSON().subscribe(data => {
      this.shIotGWPlannedTower = data;

      this.addCirclesPlannedTower();
    });
    this.appMapDataServiceService.getShIotGWPlannedJSON().subscribe(data => {
      this.shIotGWPlanned = data;

      this.addCirclesPlanned();
    });

    this.toggleLayer.subscribe(v => {
      this.toggleLayerF(v);
    });

  }

  private getBounds() {
    const bboxL = bbox(this.gridMask);
    return bboxL;
  }

  private addGrid() {
    const bboxL = [8.210906982, 55.097230334, 11.453247070, 53.156653053];
    const cellSide = 10;
    const units: Units = 'kilometers';
    const options = {units: units, mask: this.gridMask};
    // @ts-ignore
    this.squareGridFC = squareGrid(bboxL, cellSide, options);
  }

  private addCircles() {
    for (const gw of this.shIotGW['features']) {
      const coordinates = gw.geometry.coordinates.slice();

      let radius = 10;
      const options = {steps: 50, units: 'kilometers'};
      const large_circle_existing = circle(coordinates, radius, options);
      radius = 5;
      const small_circle_existing = circle(coordinates, radius, options);

      this.large_circles_existing.push(large_circle_existing);
      this.small_circles_existing.push(small_circle_existing);
    }
    this.large_circlesFC = featureCollection(this.large_circles_existing);
    this.small_circlesFC = featureCollection(this.small_circles_existing);
  }

  private addCirclesPlannedTower() {
    for (const gw of this.shIotGWPlannedTower['features']) {
      const coordinates = gw.geometry.coordinates.slice();

      let radius = 10;
      const options = {steps: 50, units: 'kilometers'};
      const large_circle_plannedTower = circle(coordinates, radius, options);
      radius = 5;
      const small_circle_plannedTower = circle(coordinates, radius, options);

      this.large_circles_plannedTower.push(large_circle_plannedTower);
      this.large_circles_plannedTower.push(small_circle_plannedTower);
    }
    this.large_circles_plannedTowerFC = featureCollection(this.large_circles_plannedTower);
    this.small_circles_plannedTowerFC = featureCollection(this.small_circles_plannedTower);
  }

  private addCirclesPlanned() {
    for (const gw of this.shIotGWPlanned['features']) {
      const coordinates = gw.geometry.coordinates.slice();

      let radius = 10;
      const options = {steps: 50, units: 'kilometers'};
      const large_circle_planned = circle(coordinates, radius, options);
      radius = 5;
      const small_circle_planned = circle(coordinates, radius, options);

      this.large_circles_planned.push(large_circle_planned);
      this.large_circles_planned.push(small_circle_planned);
    }
    this.large_circles_plannedFC = featureCollection(this.large_circles_planned);
    this.small_circles_plannedFC = featureCollection(this.small_circles_planned);
  }


  private toggleLayerF(name: string) {
    this.layouts[name] = {
      ...this.layouts[name],
      visibility: this.layouts[name].visibility === 'visible' ? 'none' : 'visible'
    };
  }
}
